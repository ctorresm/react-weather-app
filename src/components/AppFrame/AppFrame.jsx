import React, { useMemo } from 'react'
import Grid from '@material-ui/core/Grid'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import IconButton from '@material-ui/core/IconButton'
import { WiDaySunny } from 'react-icons/wi'
import Link from '@material-ui/core/Link'
import { Link as LinkRouter } from 'react-router-dom'
import { IconContext } from 'react-icons'
import Typography from '@material-ui/core/Typography'
import PropTypes from 'prop-types'


const AppFrame = ({ children }) => {
    const iconContextSize = useMemo(() => ({ size: '2em' }), [])
    return (
        <Grid container justify="center">
            <AppBar position="static">
                <Toolbar variant="dense">
                    <IconButton color="inherit" aria-label="menu">
                        <Link to="/main" component={LinkRouter} color="inherit" aria-label="menu">
                            <IconContext.Provider value={iconContextSize}>
                                <WiDaySunny />
                            </IconContext.Provider>
                        </Link>
                    </IconButton>
                    <Typography variant="h6">
                        Weather App
                    </Typography>
                </Toolbar>
            </AppBar>
            <Grid item xs={12} sm={11} md={10} lg={8}>
                {children}
            </Grid>
        </Grid>
    )
}

AppFrame.propTypes = {
    children: PropTypes.node.isRequired,
}

export default AppFrame
