import React from 'react'
import PropTypes from 'prop-types'
import { WiDayCloudy, WiDayCloudyGusts, WiDayHaze, WiDaySunny, WiFog, WiRain, WiRaindrop, WiSnow, WiThunderstorm } from 'react-icons/wi'

export const validValues = [
    "clouds",
    "clear",
    "rain",
    "snow",
    "drizzle",
    "thunderstorm",
    "mist",
    "fog",
    "haze"
]

const stateByName = {
    clouds: WiDayCloudy,
    clear: WiDaySunny,
    rain: WiRain,
    snow: WiSnow,
    drizzle: WiRaindrop,
    thunderstorm: WiThunderstorm,
    fog: WiFog,
    mist: WiDayHaze,
    haze: WiDayCloudyGusts
}

const IconState = ({ state }) => {
    const Icon = stateByName[state]
    return <Icon />
}

IconState.propTypes = {
    state: PropTypes.oneOf(validValues).isRequired
}

export default IconState
