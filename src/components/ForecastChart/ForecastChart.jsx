import React from 'react'
import PropTypes from 'prop-types'
import { LineChart, Line, XAxis, YAxis, Tooltip, Legend, ResponsiveContainer, CartesianGrid } from 'recharts'

const ForecastChart = ({ data }) => {
    return (
        <div>
            <ResponsiveContainer
                height={250}
                width={"95%"}>
                <LineChart
                    margin={{ top: 20, bottom: 20, left: 5, right: 5 }}
                    data={data}
                >
                    <XAxis dataKey="dayHour"></XAxis>
                    <YAxis></YAxis>
                    <Tooltip></Tooltip>
                    <Legend></Legend>
                    <CartesianGrid></CartesianGrid>
                    <Line type="monotone" dataKey={"max"} stroke="#ff8090"></Line>
                    <Line type="monotone" dataKey={"min"} stroke="#0000ff"></Line>
                </LineChart>
            </ResponsiveContainer>
        </div >
    )
}

ForecastChart.propTypes = {
    data: PropTypes.arrayOf(
        PropTypes.shape({
            dayHour: PropTypes.string.isRequired,
            min: PropTypes.number.isRequired,
            max: PropTypes.number.isRequired
        }),
    ).isRequired,
}

export default ForecastChart
