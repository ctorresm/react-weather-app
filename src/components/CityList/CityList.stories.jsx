import React from 'react'
import CityList from './CityList'
import { action } from '@storybook/addon-actions'

export default {
    title: "CityList",
    component: CityList
}

const cities = [
    { city: "Buenos Aires", country: "Argentina", countryCode: "AR" },
    { city: "Lima", country: "Peru", countryCode: "PE" },
    { city: "Santiago", country: "Chile", countryCode: "CL" },
    { city: "Ciudad de méxico", country: "México", countryCode: "MX" }
]

export const CityListExample = () => <CityList cities={cities} onClickCity={action("Click en city")}></CityList>
