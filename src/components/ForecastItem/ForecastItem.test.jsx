import React from 'react'
import { render } from '@testing-library/react'
import ForecastItem from './ForecastItem'

test('ForecastItem render', async () => {
    const { findByText } = render(<ForecastItem hour={10} state="clear" temperature={20} weekDay="Martes"></ForecastItem>)
    const hora = await findByText(/10/)
    const temperatura = await findByText(/20/)
    const diaSemana = await findByText(/Martes/)

    expect(diaSemana).toHaveTextContent("Martes");
    expect(hora).toHaveTextContent("10");
    expect(temperatura).toHaveTextContent("20°");

})