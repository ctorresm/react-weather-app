import React from 'react';
import ForecastItem from './ForecastItem';

export default {
    title: 'ForecastItem',
    component: ForecastItem
}

export const LunesSoleado = () => {
    return <ForecastItem hour={12} state={"sunny"} temperature={37} weekDay="Lunes" />
}