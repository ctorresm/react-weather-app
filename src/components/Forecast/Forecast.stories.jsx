import React from 'react';
import Forecast from './Forecast';

export default {
    title: "Forecast",
    component: Forecast
}

const forecastItemList = [
    { hour: 18, state: "clear", temperature: 18, weekDay: "Jueves" },
    { hour: 6, state: "clouds", temperature: 17, weekDay: "Viernes" },
    { hour: 12, state: "rain", temperature: 22, weekDay: "Sábado" },
    { hour: 13, state: "rain", temperature: 23, weekDay: "Lunes" },
    { hour: 17, state: "clear", temperature: 24, weekDay: "Jueves" }
];

export const ForecastExample = () => (<Forecast forecastItemList={forecastItemList}></Forecast>)