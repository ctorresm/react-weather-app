import React from 'react'
import Forecast from './Forecast'
import { render } from '@testing-library/react'

const forecastItemList = [
    { hour: 18, state: "clear", temperature: 18, weekDay: "Jueves" },
    { hour: 6, state: "clouds", temperature: 17, weekDay: "Viernes" },
    { hour: 12, state: "drizzle", temperature: 22, weekDay: "Sábado" },
    { hour: 13, state: "rain", temperature: 23, weekDay: "Lunes" },
    { hour: 17, state: "clear", temperature: 24, weekDay: "Jueves" }
];

test("Forecast render", async () => {

    const { findAllByTestId } = render(<Forecast forecastItemList={forecastItemList}></Forecast>)
    const forecastItems = await findAllByTestId("forecast-item-container");
    expect(forecastItems).toHaveLength(5)
})