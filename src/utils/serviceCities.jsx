
const cities = [
    { city: "Buenos Aires", country: "Argentina", countryCode: "AR" },
    { city: "Lima", country: "Peru", countryCode: "PE" },
    { city: "Bucaramanga", country: "Colombia", countryCode: "CO" },
    { city: "Ciudad de méxico", country: "México", countryCode: "MX" }
]

export const getCities = () => cities;

export const getCountryNameByCountryCode = (countryCode) => cities.filter(c => c.countryCode === countryCode)[0].country;