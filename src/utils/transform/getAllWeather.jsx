import { getCityCode } from "../utils";

const getAllWeather = (response, city, countryCode) => {

    const { data } = response;
    const temperature = Number((Number(data.main.temp) - 273.15).toFixed(0));
    const humidity = data.main.humidity;
    const wind = data.wind.speed;
    const state = (data.weather[0].main).toLowerCase();

    const propName = getCityCode(city, countryCode);
    const propValue = { temperature, state, humidity, wind }

    return ({ [propName]: propValue })
}

export default getAllWeather; 