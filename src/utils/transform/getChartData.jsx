import moment from 'moment'
import 'moment/locale/es'


const getChartData = (data) => {
    const daysAhead = [0, 1, 2, 3, 4, 5];
    const days = daysAhead.map(day => moment().add(day, 'd'));
    const dataAux = days.map(day => {

        const tempObjArray = data.list.filter(item => {
            const dayOfYear = moment.unix(item.dt).dayOfYear()
            return dayOfYear === day.dayOfYear();
        })

        const temps = tempObjArray.map(item => Math.round(Number(item.main.temp) - 273.15));

        return ({
            dayHour: day.format('ddd'),
            min: Math.min(...temps),
            max: Math.max(...temps),
            hastTemps: temps.lenght > 0 ? true : [false]
        })
    }).filter(item => item.hastTemps)
    return dataAux
}

export default getChartData;