import React, { useMemo } from 'react'
import WelcomeScreen from './../components/WelcomeScreen'
import Grid from '@material-ui/core/Grid'
import { IconContext } from 'react-icons'
import { WiDaySunny } from 'react-icons/wi'
import Typography from '@material-ui/core/Typography'
import Link from '@material-ui/core/Link'
import { Link as RouterLink } from 'react-router-dom'

const WelcomePage = props => {
    const iconContextSize = useMemo(() => ({ size: "6em" }), [])
    return (
        <div>
            <div>
                <WelcomeScreen>
                    <Grid
                        container
                        direction="column"
                        className="full"
                        justify="center">
                        <div className="highlight">
                            <Grid item
                                container
                                xs={12}
                                justify="center"
                                alignItems="center"
                                alignContent="center"
                            >
                                <Grid item>
                                    <IconContext.Provider value={iconContextSize}>
                                        <WiDaySunny></WiDaySunny>
                                    </IconContext.Provider>
                                </Grid>

                                <Grid item container direction="column" alignItems="center" justify="center">
                                    <Typography>
                                        Weather App
                                    </Typography>
                                    <Link color="inherit" component={RouterLink} to="/main">
                                        Ingresar
                                    </Link>
                                </Grid>

                            </Grid>
                        </div>
                    </Grid>
                </WelcomeScreen>
            </div>
        </div>
    )
}

WelcomePage.propTypes = {

}

export default WelcomePage
