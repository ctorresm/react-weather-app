import React, { useMemo } from 'react'
import Grid from '@material-ui/core/Grid'
import { IconContext } from 'react-icons'
import { WiRain } from 'react-icons/wi'
import Typography from '@material-ui/core/Typography'
import Link from '@material-ui/core/Link'
import { Link as RouterLink } from 'react-router-dom'

const NotFoundPage = () => {
    const iconContextSize = useMemo(() => ({ size: "6em" }), [])
    return (
        <Grid
            container
            direction="column"
            className="full"
            justify="center">
            <div className="highlight">
                <Grid item
                    container
                    xs={12}
                    justify="center"
                    alignItems="center"
                    alignContent="center"
                >
                    <Grid item>
                        <IconContext.Provider value={iconContextSize}>
                            <WiRain></WiRain>
                        </IconContext.Provider>
                    </Grid>

                    <Grid item container direction="column" alignItems="center" justify="center">
                        <Typography>
                            404 | LA PÁGINA NO EXISTE
                    </Typography>
                        <Link color="inherit" component={RouterLink} to="/">
                            Volver al inicio
                    </Link>
                    </Grid>

                </Grid>
            </div>
        </Grid>
    )
}

NotFoundPage.propTypes = {

}


export default NotFoundPage
