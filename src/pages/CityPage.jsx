import React, { useMemo } from 'react'
import { Link } from 'react-router-dom'
import Grid from '@material-ui/core/Grid'
import CityInfo from './../components/CityInfo'
import Weather from './../components/Weather'
import WeatherDetails from './../components/WeatherDetails'
import ForecastChart from './../components/ForecastChart'
import Forecast from './../components/Forecast'
import { Button } from '@material-ui/core'
import AppFrame from './../components/AppFrame'
import useCityPage from './../hooks/useCityPage'
import LinearProgress from '@material-ui/core/LinearProgress';
import useCityList from './../hooks/useCityList';
import { getCityCode } from '../utils/utils'
import { getCountryNameByCountryCode } from '../utils/serviceCities'
import { useWeatherDispatchContext, useWeatherStateContext } from '../WeatherContext'

const CityPage = () => {

    const actions = useWeatherDispatchContext();
    const data = useWeatherStateContext();

    const { allWeather, allChartData, allForecastItemList } = data;
    // const { onSetAllWeather, onSetChartData, onSetForecastItemList } = actions;
    const { city, countryCode } = useCityPage(allChartData, allForecastItemList, actions);

    const cities = useMemo(() => ([{ city, countryCode }]), [city, countryCode]);
    useCityList(cities, allWeather, actions);

    const cityCode = getCityCode(city, countryCode)

    const weather = allWeather[cityCode];
    const chartData = allChartData[cityCode];
    const forecastItemList = allForecastItemList[cityCode];

    const country = countryCode && getCountryNameByCountryCode(countryCode);
    const state = weather && weather.state;
    const temperature = weather && weather.temperature;
    const humidity = weather && weather.humidity;
    const wind = weather && weather.wind;

    return (
        <AppFrame>
            <Grid container justify="center" spacing={2}>
                <Grid container alignItems="flex-end" justify="center" item xs={12}>
                    <CityInfo city={city} country={country} ></CityInfo>
                </Grid>
                <Grid container direction="column" alignItems="center" justify="center">
                    <Grid item >
                        <Weather state={state} temperature={temperature}></Weather>
                    </Grid>
                    <Grid item >
                        {
                            humidity && wind && <WeatherDetails humidity={humidity} wind={wind} />
                        }
                    </Grid>
                </Grid>

                <Grid item xs={12}>

                    {!chartData && !forecastItemList && <LinearProgress color="secondary" />}
                </Grid>

                <Grid item xs={12}>
                    {
                        chartData && <ForecastChart data={chartData}></ForecastChart>
                    }
                </Grid>
                <Grid item xs={12}>
                    {
                        forecastItemList && <Forecast forecastItemList={forecastItemList}></Forecast>
                    }
                </Grid>
            </Grid>
            <Link to="/main">
                <Button>
                    Volver a la lista de cuidades
                </Button>
            </Link>
        </AppFrame >
    )
}

CityPage.propTypes = {

}

export default CityPage
