import React, { Suspense, lazy } from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import { WeatherContext } from './WeatherContext';

const Welcome = lazy(() => import('./pages/WelcomePage'));
const Main = lazy(() => import('./pages/MainPage'));
const City = lazy(() => import('./pages/CityPage'));
const NotFoundPage = lazy(() => import('./pages/NotFoundPage'));

const App = () => {

    return (
        <WeatherContext>
            <Router>
                <Suspense fallback={<div>Loading...</div>}>
                    <Switch>
                        <Route path='/' exact>
                            <Welcome></Welcome>
                        </Route>
                        <Route path='/main'>
                            <Main ></Main>
                        </Route>
                        <Route path='/city/:countryCode/:city' >
                            <City ></City>
                        </Route>
                        <Route >
                            <NotFoundPage></NotFoundPage>
                        </Route>
                    </Switch>
                </Suspense>
            </Router>
        </WeatherContext>
    )
}

export default App
