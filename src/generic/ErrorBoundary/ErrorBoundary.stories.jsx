import React from 'react'
import ErrorBoundary from './ErrorBoundary'

export default {
    title: "ErrorBoundary",
    component: ErrorBoundary
}

const ComponentWithoutError = () => <h1>Sin error</h1>

const ComponentWithError = () => <h1>{(undefined).hola}</h1>

export const ErrorBoundayWithError = () =>
    (
        <ErrorBoundary >
            <ComponentWithError />
        </ErrorBoundary>
    )

export const ErrorBoundayWithoutError = () =>
    (
        <ErrorBoundary >
            <ComponentWithoutError />
        </ErrorBoundary>
    )